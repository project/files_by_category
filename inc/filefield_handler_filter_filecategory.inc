<?php
/**
 * @file
 * Files By Category: Add a new filter in files view.
 */

class filefield_handler_filter_filecategory extends views_handler_filter_many_to_one {
  function has_extra_options() { return TRUE; }

  function get_value_options() { /* don't overwrite the value options */ }

  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'textfield');
    $options['limit'] = array('default' => TRUE);
    $options['vid'] = array('default' => 0);

    return $options;
  }
  
  
  function extra_options_form(&$form, &$form_state) {
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $voc) {
      $options[$voc->vid] = check_plain($voc->name);
    }

    if ($this->options['limit']) {
      // We only do this when the form is displayed.
      if ($this->options['vid'] == 0) {
        $first_vocabulary = reset($vocabularies);
        $this->options['vid'] = $first_vocabulary->vid;
      }

      $form['vid'] = array(
        '#prefix' => '<div class="views-left-40">',
        '#suffix' => '</div>',
        '#type' => 'radios',
        '#title' => t('Vocabulary'),
        '#options' => $options,
        '#description' => t('Select which vocabulary to show terms for in the regular options.'),
        '#default_value' => $this->options['vid'],
      );
    }

    $form['markup_start'] = array(
      '#value' => '<div class="views-left-40">',
    );
    $form['markup_end'] = array(
      '#value' => '</div>',
    );
  }




  function value_form(&$form, &$form_state) {
  
  $terms = taxonomy_get_tree($this->options['vid']);
  foreach ($terms as $term) {
    $options[$term->name] = $term->name;
  }
    $form['value'] = array(
    '#type' => 'select',
    '#title' => t('Category'),
    '#options' => $options,
    );
 
    return $form;  
  }

}

