<?php
/**
 * @file
 * Files By Category: Add a new field in files view.
 */
 
class filefield_handler_field_filecategory extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['fid'] = 'fid';
    $this->additional_fields['filename'] = 'filename';
    $this->additional_fields['filesize'] = 'filesize';
	$this->additional_fields['filecategory'] = 'filecategory';
  }

  function render($values) {
    $pseudo_file = array(
      'fid' => $values->{$this->aliases['fid']},
      'filemime' => $values->{$this->field_alias},
      'filename' => $values->{$this->aliases['filename']},
      'filesize' => $values->{$this->aliases['filesize']},
	  'filecategory' => $values->{$this->aliases['filecategory']},
    );
	return $values->{$this->aliases['filecategory']};
  }
}
